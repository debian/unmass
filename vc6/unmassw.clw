; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CUnmasswDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Unmassw.h"

ClassCount=4
Class1=CUnmasswApp
Class2=CUnmasswDlg

ResourceCount=4
Resource2=IDD_DIALOGarchiveType
Resource1=IDR_MAINFRAME
Class3=CUnmassAboutDlg
Resource3=IDD_DIALOGabout
Class4=CChArchType
Resource4=IDD_UNMASSW_DIALOG (English (U.S.))

[CLS:CUnmasswApp]
Type=0
HeaderFile=Unmassw.h
ImplementationFile=Unmassw.cpp
Filter=N
LastObject=CUnmasswApp

[CLS:CUnmasswDlg]
Type=0
HeaderFile=UnmasswDlg.h
ImplementationFile=UnmasswDlg.cpp
Filter=D
LastObject=CUnmasswDlg
BaseClass=CDialog
VirtualFilter=dWC



[DLG:IDD_UNMASSW_DIALOG (English (U.S.))]
Type=1
Class=CUnmasswDlg
ControlCount=18
Control1=IDC_BUTTONopen,button,1342242817
Control2=IDC_BUTTONdir,button,1342275840
Control3=IDC_LISTfiles,SysListView32,1350631433
Control4=IDC_BUTTONextract,button,1342242816
Control5=IDC_BUTTONaddFile,button,1476460544
Control6=IDC_BUTTONaddDir,button,1476460544
Control7=IDC_BUTTONunlinkFile,button,1476460544
Control8=IDC_BUTTONdelFile,button,1208025088
Control9=IDC_BUTTONcreateArchive,button,1342242816
Control10=IDC_BUTTONselAdd,button,1342242816
Control11=IDC_BUTTONselRem,button,1342242816
Control12=IDC_COMBOselTypes,combobox,1344340227
Control13=IDC_BUTTONabout,button,1342242816
Control14=IDC_STATICinfo2,static,1342308352
Control15=IDC_STATICinfo,static,1342308352
Control16=IDC_PROGRESS1,msctls_progress32,1350565889
Control17=IDC_STATIC,static,1342177294
Control18=IDC_STATIC,static,1342177294

[DLG:IDD_DIALOGabout]
Type=1
Class=CUnmassAboutDlg
ControlCount=7
Control1=IDOK,button,1342242817
Control2=IDC_LIST1,listbox,1344356609
Control3=IDC_STATICtxt1,static,1342308352
Control4=IDC_STATIC,static,1342308352
Control5=IDC_STATIC,static,1342308352
Control6=IDC_STATIC,static,1342308352
Control7=IDC_EDITgreets,edit,1352730693

[CLS:CUnmassAboutDlg]
Type=0
HeaderFile=UnmassAboutDlg.h
ImplementationFile=UnmassAboutDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=CUnmassAboutDlg
VirtualFilter=dWC

[DLG:IDD_DIALOGarchiveType]
Type=1
Class=CChArchType
ControlCount=3
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_COMBOtype,combobox,1344340227

[CLS:CChArchType]
Type=0
HeaderFile=ChArchType.h
ImplementationFile=ChArchType.cpp
BaseClass=CDialog
Filter=D
LastObject=CChArchType
VirtualFilter=dWC

