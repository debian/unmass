//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Unmassw.rc
//
#define IDD_UNMASSW_DIALOG              102
#define IDR_MAINFRAME                   128
#define IDD_DIALOGabout                 129
#define IDR_MAINFRAME3                  129
#define IDB_BITMAP_liana                139
#define IDB_BITMAProh2vod               140
#define IDB_BITMAProh1zvis              141
#define IDD_DIALOGarchiveType           142
#define IDC_LISTfiles                   1000
#define IDC_BUTTONopen                  1001
#define IDC_BUTTONextract               1002
#define IDC_STATICinfo                  1003
#define IDC_COMBOselTypes               1004
#define IDC_BUTTONdir                   1005
#define IDC_BUTTONabout                 1006
#define IDC_LIST1                       1007
#define IDC_BUTTONselAdd                1007
#define IDC_PROGRESS1                   1008
#define IDC_STATICtxt1                  1008
#define IDC_STATICinfo2                 1009
#define IDC_EDITgreets                  1009
#define IDC_BUTTONselRem                1010
#define IDC_COMBOtype                   1010
#define IDC_BUTTONaddFile               1011
#define IDC_BUTTONaddDir                1012
#define IDC_BUTTONunlinkFile            1013
#define IDC_BUTTONdelFile               1014
#define IDC_BUTTONcreateArchive         1015

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        143
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1017
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
