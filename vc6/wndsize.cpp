#include "stdafx.h"
#include <afxwin.h>
#include <assert.h>

#include "WndSize.h"

 CWndSize::CWndSize()
{
	mainwnd = NULL;
}

void CWndSize::SetWindow( CWnd *wnd )
{
	mainwnd = wnd;
	CheckWindowStyle();
}

void CWndSize::CheckWindowStyle( void )
{
	if ( mainwnd == NULL )
		return;
/*
	DWORD style = mainwnd->GetStyle(); 

	style = style & ( WS_BORDER | WS_DLGFRAME | WS_THICKFRAME );

	xedge = yedge = 0;

	if ( style == WS_BORDER ) {
		xedge = 1;
		yedge = 1;
	}
	if ( style == ( WS_BORDER | WS_DLGFRAME ) ) {
		xedge = 3;
		yedge = 22;
	}
	if ( style == ( WS_BORDER | WS_DLGFRAME | WS_THICKFRAME ) ) {
		xedge = 4;
		yedge = 23;
	}
*/
	CPoint		p;
	CRect		r;

	p.x = p.y = 0;
	mainwnd->ClientToScreen( &p );
	mainwnd->GetWindowRect( &r );
	xedge = p.x - r.left;
	yedge = p.y - r.top;
}

void CWndSize::SetWndEdgeAssignment( int itemid, 
	pos_from froml, pos_from fromr, pos_from fromu, pos_from fromd )
{
	assert( mainwnd != NULL );
	if ( mainwnd == NULL )
		return;

	CRect	main, wnd;
	int		i;

	mainwnd->GetWindowRect( &main );
	mainwnd->GetDlgItem( itemid )->GetWindowRect( &wnd );

	WndData.id					= itemid;
	WndData.edge[ left ].from	= froml;
	WndData.edge[ right ].from	= fromr;
	WndData.edge[ top ].from	= fromu;
	WndData.edge[ bottom ].from	= fromd;

	for( i=0; i<sides; i++ )
		assert(( WndData.edge[ bottom ].from == top_left ) || 
				( WndData.edge[ bottom ].from == right_bottom ));

	if ( froml == top_left )
		WndData.edge[ left ].pos	= wnd.left;
	else
		WndData.edge[ left ].pos	= main.right - wnd.left;
	if ( fromr == top_left )
		WndData.edge[ right ].pos	= wnd.right;
	else
		WndData.edge[ right ].pos	= main.right - wnd.right;
	if ( fromu == top_left )
		WndData.edge[ top ].pos		= wnd.top;
	else
		WndData.edge[ top ].pos		= main.bottom - wnd.top;
	if ( fromd == top_left )
		WndData.edge[ bottom ].pos	= wnd.bottom;
	else
		WndData.edge[ bottom ].pos	= main.bottom - wnd.bottom;

	data.push_back( WndData );
}

void CWndSize::Size( void )
{
	RECT		wRect, r2;
	int			sx, sy, i, j;
	long		s[ 4 ];

	if ( mainwnd == NULL )
		return;

	//zistim velkost okna
	mainwnd->GetWindowRect( &wRect );
	sx = wRect.right - wRect.left;
	sy = wRect.bottom - wRect.top;

	//posuniem okna podla velkosti hlavneho okna
	for( i=0; i<data.size(); i++ ) {

		WndData = data[ i ];

		for( j=0; j<sides; j++ ) {
			switch ( WndData.edge[ j ].from ) {
			case top_left:
				s[ j ] = WndData.edge[ j ].pos;
				break;
			case right_bottom:
				if (( j == right ) || ( j == left ))
					s[ j ] = sx - WndData.edge[ j ].pos;
				if (( j == bottom ) || ( j == top ))
					s[ j ] = sy - WndData.edge[ j ].pos;
				break;
			}
		}

		s[ right ] -= s[ left ]; 
		s[ bottom ] -= s[ top ];
		s[ left ] -= xedge;
		s[ top ] -= yedge;

		mainwnd->GetDlgItem( WndData.id )->MoveWindow( 
			s[ left ], s[ top ], s[ right ], s[ bottom ], false );


		mainwnd->GetDlgItem( WndData.id )->GetWindowRect( &r2 );
		mainwnd->ClientToScreen( &r2 );

	}

}