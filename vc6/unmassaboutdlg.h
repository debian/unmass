#if !defined(AFX_UNMASSABOUTDLG_H__D2052740_20B9_11D6_90B4_52544CC08087__INCLUDED_)
#define AFX_UNMASSABOUTDLG_H__D2052740_20B9_11D6_90B4_52544CC08087__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UnmassAboutDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CUnmassAboutDlg dialog

class CUnmassAboutDlg : public CDialog
{
// Construction
public:
	CUnmassAboutDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CUnmassAboutDlg)
	enum { IDD = IDD_DIALOGabout };
	CListBox	m_list;
	CString	m_greets;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUnmassAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CUnmassAboutDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UNMASSABOUTDLG_H__D2052740_20B9_11D6_90B4_52544CC08087__INCLUDED_)
