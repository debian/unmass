// UnmassAboutDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Unmassw.h"
#include "massfs.h"
#include "UnmassAboutDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUnmassAboutDlg dialog


CUnmassAboutDlg::CUnmassAboutDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUnmassAboutDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CUnmassAboutDlg)
	m_greets = _T("");
	//}}AFX_DATA_INIT
}


void CUnmassAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUnmassAboutDlg)
	DDX_Control(pDX, IDC_LIST1, m_list);
	DDX_Text(pDX, IDC_EDITgreets, m_greets);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CUnmassAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CUnmassAboutDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUnmassAboutDlg message handlers

BOOL CUnmassAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CMassFiles	mass;
	int		i;

	UpdateData();

	GetDlgItem( IDC_STATICtxt1 )->SetWindowText( NAME_VER" version, created by Mirex" );

	for( i=0; i<mass.massftypes_count; i++ )
		m_list.AddString( mass.GetTypeIdentString( i ) );

	m_list.AddString( "(b = beta, not tested much)" );
	m_list.AddString( "(a = alfa, not tested at all, no test files available)" );


	m_greets = "Thanks:\r\n";
	m_greets += "Ian Patterson - for releasing OniTools sourcecode, which helped me with Oni\r\n";
	m_greets += "Ficedula - for releasing info on lzss compression (used in ff8 unpacking)\r\n";
	
	m_greets += "\r\ngreets:\r\nStanLee - Alhexx - Elbereth - Sephiroth 3D - Rubicant";

	UpdateData( false );

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
