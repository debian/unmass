// UnmasswDlg.h : header file
//

#if !defined(AFX_UNMASSWDLG_H__F83C94E5_1FE5_11D6_90B4_52544CC08087__INCLUDED_)
#define AFX_UNMASSWDLG_H__F83C94E5_1FE5_11D6_90B4_52544CC08087__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "stdafx.h"
#include "massfs.h"
#include "ma.h"
#include "WndSize.h"

/////////////////////////////////////////////////////////////////////////////
// CUnmasswDlg dialog

class CUnmasswDlg : public CDialog
{
// Construction
public:
	CUnmasswDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CUnmasswDlg)
	enum { IDD = IDD_UNMASSW_DIALOG };
	CComboBox	m_SelTypes;
	CProgressCtrl	m_progress;
	CButton	m_DirButton;
	CListCtrl	m_list;
	CString	m_info;
	CString	m_info2;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUnmasswDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON	icon;

	// Generated message map functions
	//{{AFX_MSG(CUnmasswDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBUTTONopen();
	afx_msg void OnBUTTONextract();
	afx_msg void OnBUTTONdir();
	afx_msg void OnDestroy();
	afx_msg void OnBUTTONabout();
	afx_msg void OnColumnclickLISTfiles(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDblclkLISTfiles(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBUTTONselAdd();
	afx_msg void OnBUTTONselRem();
	afx_msg void OnBUTTONaddFile();
	afx_msg void OnBUTTONaddDir();
	afx_msg void OnBUTTONunlinkFile();
	afx_msg void OnBUTTONdelFile();
	afx_msg void OnBUTTONcreateArchive();
	virtual void OnCancel();
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void InitWndPositions( void );
	void DoSelect( int select );
	void NewFileIntoList( unsigned long listpos );
	int ReopenFileForWriting( void );
	void CloseArchive();
	int OpenArchive( char* filename );

	CMassFiles		mass;
	CString			OutputPath;

#define MaxPathLength 512
	char			OpenDir[ MaxPathLength ], OutputDir[ MaxPathLength ];

#define MaxExtensionLength 30

	struct s_FileInfo {
		CMassArchive::s_FileRec		FileRec;
//		char						ext[ MaxExtensionLength ];
	} *FileInfo;

	unsigned long	*InListPos;			// ILP[0] = cislo itemu ktory je v liste na pos 0

	enum e_sort {
		sort_unsorted,
		sort_name,
		sort_size,
		sort_type,
		sort_undefined
	} sort, sort_displayed;

	void FillListSorted( e_sort sorttype );

	CWndSize	SizeCtrls;

	char	ProgramPath[ 512 ], TempDir[ 512 ]; // no end slashes

	int		NoExtInCombo;

	int		ArchiveOpened;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UNMASSWDLG_H__F83C94E5_1FE5_11D6_90B4_52544CC08087__INCLUDED_)
