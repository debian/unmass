# Microsoft Developer Studio Project File - Name="unmassc" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=unmassc - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "unmassc.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "unmassc.mak" CFG="unmassc - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "unmassc - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "unmassc - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "unmassc - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\temp\unmassc\Release"
# PROP Intermediate_Dir "..\temp\unmassc\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x41b /d "NDEBUG"
# ADD RSC /l 0x41b /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386

!ELSEIF  "$(CFG)" == "unmassc - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\temp\unmassc\Debug"
# PROP Intermediate_Dir "..\temp\unmassc\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD BASE RSC /l 0x41b /d "_DEBUG"
# ADD RSC /l 0x41b /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "unmassc - Win32 Release"
# Name "unmassc - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\unmassc.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# Begin Group "Archives"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\ma.cpp
# End Source File
# Begin Source File

SOURCE=.\ma.h
# End Source File
# Begin Source File

SOURCE=.\ma_bif.cpp
# End Source File
# Begin Source File

SOURCE=.\ma_bif.h
# End Source File
# Begin Source File

SOURCE=.\Ma_crism.cpp
# End Source File
# Begin Source File

SOURCE=.\Ma_crism.h
# End Source File
# Begin Source File

SOURCE=.\Ma_dune2.cpp
# End Source File
# Begin Source File

SOURCE=.\Ma_dune2.h
# End Source File
# Begin Source File

SOURCE=.\ma_ecou.cpp
# End Source File
# Begin Source File

SOURCE=.\ma_ecou.h
# End Source File
# Begin Source File

SOURCE=.\ma_eth2.cpp
# End Source File
# Begin Source File

SOURCE=.\ma_eth2.h
# End Source File
# Begin Source File

SOURCE=.\ma_ff8.cpp
# End Source File
# Begin Source File

SOURCE=.\ma_ff8.h
# End Source File
# Begin Source File

SOURCE=.\ma_fpk.cpp
# End Source File
# Begin Source File

SOURCE=.\ma_fpk.h
# End Source File
# Begin Source File

SOURCE=.\Ma_grp.cpp
# End Source File
# Begin Source File

SOURCE=.\Ma_grp.h
# End Source File
# Begin Source File

SOURCE=.\Ma_gunme.cpp
# End Source File
# Begin Source File

SOURCE=.\Ma_gunme.h
# End Source File
# Begin Source File

SOURCE=.\Ma_ipwad.cpp
# End Source File
# Begin Source File

SOURCE=.\Ma_ipwad.h
# End Source File
# Begin Source File

SOURCE=.\Ma_lbx.cpp
# End Source File
# Begin Source File

SOURCE=.\Ma_lbx.h
# End Source File
# Begin Source File

SOURCE=.\Ma_lgp.cpp
# End Source File
# Begin Source File

SOURCE=.\ma_lgp.h
# End Source File
# Begin Source File

SOURCE=.\ma_mea.cpp
# End Source File
# Begin Source File

SOURCE=.\ma_mea.h
# End Source File
# Begin Source File

SOURCE=.\ma_mgmnl.cpp
# End Source File
# Begin Source File

SOURCE=.\ma_mgmnl.h
# End Source File
# Begin Source File

SOURCE=.\ma_mgs.cpp
# End Source File
# Begin Source File

SOURCE=.\ma_mgs.h
# End Source File
# Begin Source File

SOURCE=.\Ma_moor3.cpp
# End Source File
# Begin Source File

SOURCE=.\Ma_moor3.h
# End Source File
# Begin Source File

SOURCE=.\ma_oni_d.cpp
# End Source File
# Begin Source File

SOURCE=.\ma_oni_d.h
# End Source File
# Begin Source File

SOURCE=.\Ma_pak.cpp
# End Source File
# Begin Source File

SOURCE=.\ma_pak.h
# End Source File
# Begin Source File

SOURCE=.\Ma_pbo.cpp
# End Source File
# Begin Source File

SOURCE=.\Ma_pbo.h
# End Source File
# Begin Source File

SOURCE=.\Ma_roll.cpp
# End Source File
# Begin Source File

SOURCE=.\Ma_roll.h
# End Source File
# Begin Source File

SOURCE=.\Ma_swine.cpp
# End Source File
# Begin Source File

SOURCE=.\Ma_swine.h
# End Source File
# Begin Source File

SOURCE=.\Ma_umod.cpp
# End Source File
# Begin Source File

SOURCE=.\Ma_umod.h
# End Source File
# Begin Source File

SOURCE=.\ma_vf1bi.cpp
# End Source File
# Begin Source File

SOURCE=.\ma_vf1bi.h
# End Source File
# Begin Source File

SOURCE=.\Ma_vol.cpp
# End Source File
# Begin Source File

SOURCE=.\Ma_vol.h
# End Source File
# Begin Source File

SOURCE=.\Ma_wad2.cpp
# End Source File
# Begin Source File

SOURCE=.\Ma_wad2.h
# End Source File
# Begin Source File

SOURCE=.\Ma_wtn.cpp
# End Source File
# Begin Source File

SOURCE=.\Ma_wtn.h
# End Source File
# Begin Source File

SOURCE=.\Massfs.cpp
# End Source File
# Begin Source File

SOURCE=.\Massfs.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
