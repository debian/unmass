#ifndef _CWndSize_h_
#define _CWndSize_h_

#include <afxwin.h>
#include <vector>

	enum pos_from {
		top_left,
		right_bottom,
	} ;

class CWndSize {

private:
	enum {
		left,
		right,
		top,
		bottom,
		sides
	} ;

	struct s_WndData{
		struct {
			unsigned int	pos;
			pos_from		from;
		} edge[ 4 ];
		int		id;
	} WndData;

	std::vector< s_WndData >	data;

	int				xedge, yedge;
	
	CWnd			*mainwnd;

public:
			CWndSize();

	void	SetWindow( CWnd *wnd );
	//call if window's border was changed or menu was added/removed
	void	CheckWindowStyle( void );

	void	SetWndEdgeAssignment( int itemid, 
				pos_from froml, pos_from fromr, pos_from fromu, pos_from fromd );
	void	Size( void );

	
};

#endif