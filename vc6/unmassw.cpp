// Unmassw.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Unmassw.h"
#include "UnmasswDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUnmasswApp

BEGIN_MESSAGE_MAP(CUnmasswApp, CWinApp)
	//{{AFX_MSG_MAP(CUnmasswApp)
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUnmasswApp construction

CUnmasswApp::CUnmasswApp()
{
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CUnmasswApp object

CUnmasswApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CUnmasswApp initialization

BOOL CUnmasswApp::InitInstance()
{
	// Standard initialization

	CUnmasswDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
	}
	else if (nResponse == IDCANCEL)
	{
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
