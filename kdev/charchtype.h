#if !defined(AFX_CHARCHTYPE_H__ACD2354B_C12F_11D7_8318_CB364A5EFC4F__INCLUDED_)
#define AFX_CHARCHTYPE_H__ACD2354B_C12F_11D7_8318_CB364A5EFC4F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ChArchType.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CChArchType dialog

class CChArchType : public CDialog
{
// Construction
public:
	CChArchType(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CChArchType)
	enum { IDD = IDD_DIALOGarchiveType };
	CComboBox	m_ArchiveType;
	//}}AFX_DATA

	CStringArray	strings;
	CDWordArray		data;

	int				choice;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChArchType)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CChArchType)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHARCHTYPE_H__ACD2354B_C12F_11D7_8318_CB364A5EFC4F__INCLUDED_)
