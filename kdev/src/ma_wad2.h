#ifndef _cmass_wad2_h
#define _cmass_wad2_h

#include "ma.h"

class CMassWad2 : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next
	int  Extract( unsigned char *output );

	#define wad2NameLen             16

    struct s_wad2Rec{
        unsigned long   offset;
		unsigned long   size;
		unsigned long   u1, u2;
		char            name[ wad2NameLen ];
	} wad2Rec;      //len:32

	#define SizeOfWad2Rec 32
} ;

#endif
