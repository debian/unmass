#ifndef _cmass_bif_h
#define _cmass_bif_h

#include "ma.h"

class CMassBif : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next

	struct s_bifRec {
		short			number;
		short			u1;
		unsigned long	offset;
		unsigned long	size;
		unsigned long	u2;
	} bifRec;       //len:16

	#define SizeOfBifRec 16

	unsigned char	buf[ 10 ];
	char			ext[ 5 ];
} ;

#endif
