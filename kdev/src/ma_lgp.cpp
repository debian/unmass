#include "ma_lgp.h"

char *CMassLgp::GetIdent( void )
{
	static char Ident[] = "LGP (Final Fantasy 7)";

	return Ident;
}

char *CMassLgp::GetExtension( void )
{
	static char Ext[] = "lgp";
	return Ext;
}

int	 CMassLgp::CheckFileType( void )
{
	if ( massf == NULL )
		return 0;

	char	buf[ 100 ];
	fseek( massf, 0, SEEK_SET );
	fread( buf, 12, 1, massf );

	if ( memcmp( buf, "\0\0SQUARESOFT", 12 ) == 0 )
		return 1;
	else
		return 0;
}

void CMassLgp::GetFileMainInfo( void )
{
	files_count = 0;

	if ( massf == NULL )
		return;

	fseek( massf, 0, SEEK_END );
	filesize = ftell( massf );

	fseek( massf, 12, SEEK_SET );
	fread( &files_count, 4, 1, massf );
	lib_start = 16;
}

int	 CMassLgp::ReadRec( long num )
{
	if ( massf == NULL ) 
		return 0;
	if (( (unsigned long)num >= files_count ) && ( num != -1 ))
		return 0;
	if (( num == -1 ) && 
		(( ! next_set ) || ((unsigned long)next_rec_num >= files_count )) )
		return 0;
	
	memset( &FileRec, 0, sizeof( FileRec ) );

	//seek
	if ( num == -1 ) {
		if ( fseek( massf, next_rec_pos, SEEK_SET ) != 0 )
			return 0;
	}
	else {
		pos = lib_start + SizeOfLgpRec * num;
		if ( fseek( massf, pos, SEEK_SET ) != 0 )
			return 0;
	}

	//read
	if ( fread( &lgpRec, SizeOfLgpRec, 1, massf ) != 1 )
		return 0;

	//set position of the next
	next_rec_pos = ftell( massf );
	if ( num == -1 )
		next_rec_num ++;
	else
		next_rec_num = num + 1;
	if ( num + 1 >= (long)files_count )
		next_set = false;
	else
		next_set = true;
	
	//get info
	FileRec.SetName( lgpRec.name );
	FileRec.offset = lgpRec.offset + 24;
	if ( lgpRec.offset + 24 < filesize ) {
		if ( fseek( massf, lgpRec.offset + 20, SEEK_SET ) != 0 ) {
			SetErrorStr( "ReadRec: lgp seek to filesize\n" );
			return 0;
		}
		if ( fread( &FileRec.size, 4, 1, massf ) != 1 ) {
			SetErrorStr( "ReadRec: lgp read filesize" );
			return 0;
		}
		FileRec.set = 1;
	}
	else
		return 0;

	return 1;
}

