#include "ma_bif.h"

char *CMassBif::GetIdent( void )
{
	static char Ident[] = "BIF (Baldur's Gate 2)";

	return Ident;
}

char *CMassBif::GetExtension( void )
{
	static char Ext[] = "bif";
	return Ext;
}

int	 CMassBif::CheckFileType( void )
{
	if ( massf == NULL )
		return 0;

	char	buf[ 100 ];
	fseek( massf, 0, SEEK_SET );
	fread( buf, 8, 1, massf );

	if ( memcmp( buf, "BIFFV1  ", 8 ) == 0 )
		return 1;
	else
		return 0;
}

void CMassBif::GetFileMainInfo( void )
{
	files_count = 0;

	if ( massf == NULL )
		return;

	fseek( massf, 0, SEEK_END );
	filesize = ftell( massf );

	fseek( massf, 8, SEEK_SET );
	fread( &files_count, 4, 1, massf );
	lib_start = 20;
}

int	 CMassBif::ReadRec( long num )
{
	if ( massf == NULL ) 
		return 0;
	if (( (unsigned long)num >= files_count ) && ( num != -1 ))
		return 0;
	if (( num == -1 ) && 
		(( ! next_set ) || ((unsigned long)next_rec_num >= files_count )) )
		return 0;
	
	memset( &FileRec, 0, sizeof( FileRec ) );

	if (( next_set ) && ( (signed)next_rec_num == num ))
		num = -1;

	//seek
	if ( num == -1 ) {
		if ( fseek( massf, next_rec_pos, SEEK_SET ) != 0 )
			return 0;
	}
	else {
		pos = lib_start + SizeOfBifRec * num;
		if ( fseek( massf, pos, SEEK_SET ) != 0 )
			return 0;
	}

	//read
	if ( fread( &bifRec, SizeOfBifRec, 1, massf ) != 1 )
		return 0;

	//set position of the next
	next_rec_pos = ftell( massf );
	if ( num == -1 )
		next_rec_num ++;
	else
		next_rec_num = num + 1;
	if ( num + 1 >= (long)files_count )
		next_set = false;
	else
		next_set = true;
	
	//get info

	//name from file head.
	strcpy( ext, "" );
	fseek( massf, bifRec.offset, SEEK_SET );
	fread( buf, 1, 10, massf );
	if ( memcmp( buf, "BM", 2 ) == 0 )
		strcpy( ext, "bmp" );
//	if ( memcmp( buf, "WAV", 2 ) == 0 )
//		strcat( FileRec.name, ".wav" );

	sprintf( FileRec.name, "file%04lu.%s", next_rec_num - 1, ext );

	FileRec.offset = bifRec.offset;
	FileRec.size = bifRec.size;
	FileRec.set = 1;

	return 1;
}
