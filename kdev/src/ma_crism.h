#ifndef _cmass_Crismon_h
#define _cmass_Crismon_h

#include "ma.h"

class CMassCrismon : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next

	struct s_crismonRec {
		char            name[ FileNameWithPathMaxLen ];
		unsigned long	offset;
		unsigned long	size;
	} crismonRec;

} ;

#endif
