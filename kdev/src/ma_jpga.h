#ifndef _cmass_jpga_h
#define _cmass_jpga_h

// for jpeg archives, for example pps's

#include "ma.h"

class CMassJpga : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next
	int  Extract( unsigned char *output );

} ;

#endif
