#ifndef _cmass_dune2_h
#define _cmass_dune2_h

#include "ma.h"

class CMassDune2 : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next
	int  Extract( unsigned char *output );

	#define dune2NameLen	FileNameWithPathMaxLen

	struct s_dune2Rec {
			unsigned long	offset;
			char			name[ dune2NameLen ];
	} dune2Rec; // 4 + name + 1(zero)

	#define SizeOfDune2Rec 27
} ;

#endif
