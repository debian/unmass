#ifndef _cmass_wtn_h
#define _cmass_wtn_h

#include "ma.h"

class CMassWtn : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next
	unsigned long Extract( unsigned long offset, unsigned long size, unsigned char *output );

	#define wtnNameLen FileNameWithPathMaxLen

	struct s_wtnRec {
		long			rec_type;
		unsigned char	u1;
		unsigned long	name_len;
		char			name[ wtnNameLen ];
		unsigned long	subfiles_count;
		unsigned long	offset;
		unsigned long	size;
		long			u2;
	} wtnRec;		//len:25+name

	#define SizeOfWtnRec 25
} ;

#endif
