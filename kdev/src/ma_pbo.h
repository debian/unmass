#ifndef _cmass_pbo_h
#define _cmass_pbo_h

#include "ma.h"

class CMassPbo : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next
	int  Extract( unsigned char *output );

	#define pboNameLen FileNameWithPathMaxLen

	struct s_pboRec {
			char 			name[ pboNameLen ];
			unsigned char 	u[12];
			unsigned long 	u2;
			unsigned long 	size;
	} pboRec; //20 + namelen

	#define SizeOfPboRec 20
} ;

#endif
