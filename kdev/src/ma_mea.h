// MEA Mirex Exe Archive

#ifndef _cmass_mea_h
#define _cmass_mea_h

#include "ma.h"

class CMassMea : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next
	int	AddFileRecord( void );
//	int AddFileData( unsigned long offset, unsigned long size, char* data );
	int GetFlags( void );
	int	UnlinkFile( unsigned long index );
	int WriteLib( void );
	int CreateNewArchive( FILE *newf );

	#define MeaNameLen	64
 
	struct s_MeaRec {
		char			name[ MeaNameLen ];
		unsigned long	offset;
		unsigned long	size;
	} MeaRec;       //len:72

	struct s_MeaHeader {
		unsigned long	orig_file_size;
		unsigned long	lib_offset;
		unsigned long	files_count;
		unsigned long	file_size;
		char			ident[4];
	} MeaHeader;

	#define SizeOfMeaRec 72

	int	EmptyExe;

} ;

#endif
