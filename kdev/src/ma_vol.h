#ifndef _cmass_vol_h
#define _cmass_vol_h

#include "ma.h"

class CMassVol : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next
	int  Extract( unsigned char *output );

	struct s_volRec {
		unsigned long	offset;
	} volRec;		//len:4

	#define SizeOfVolRec 4
} ;

#endif
