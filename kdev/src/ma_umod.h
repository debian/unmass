#ifndef _cmass_umod_h
#define _cmass_umod_h

#include "ma.h"

class CMassUmod : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next
	int  Extract( unsigned char *output );

	#define umodNameLen	FileNameWithPathMaxLen
 
	struct s_umodRec {
		unsigned char	namelen;
		char			name[ umodNameLen ];
		unsigned long	offset;
		unsigned long	size;
		unsigned long	u;
	} umodRec; // 14 + namelen

	#define SizeOfUmodRec 14
} ;

#endif
