#ifndef _cmass_swine_h
#define _cmass_swine_h

#include "ma.h"

class CMassSwine : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next
	int  Extract( unsigned char *output );

	#define swineNameLen FileNameWithPathMaxLen

	struct s_swineRec {
		char			u1;
		unsigned char	name_len;
		char			name[ swineNameLen ];
		unsigned long	offset;
		unsigned long	size;
		char			u2[5];
	} swineRec; // 15 + namelen

	#define SizeOfSwineRec 15
} ;

#endif
