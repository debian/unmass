#ifndef _cmass_oni_dat_h
#define _cmass_oni_dat_h

#include "ma.h"

class CMassOniDat : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next
	int		GetFlags( void );
	unsigned long Extract( unsigned long offset, unsigned long size, unsigned char *output );

	struct s_OniDatRec {
		union {
			char			type[ 4 ];
			unsigned long	typel;
		} ;
		unsigned long	offset, name_offset, size;
		long			flags;
	} OniDatRec;       //len:20

	#define SizeOfOniDatRec 20

	struct {
		unsigned long	u1[5];
		unsigned long	file_list_elements;
		unsigned long	name_list_elements;
		unsigned long	u2;
		unsigned long	file_data_offset;	// - 8
 		unsigned long	file_data_size	;
		unsigned long	name_table_offset;
		unsigned long	name_table_size;
		unsigned long	u4[4];
	} OniDatHeader;
	//size = 64b

	char	RawFileName[ FileNameWithPathMaxLen ];
	char	str[ FileNameWithPathMaxLen ], str2[ FileNameWithPathMaxLen ];
	FILE	*fraw;
	int		i;

	//file structs
	struct
	{
			unsigned long	id;
			unsigned long	unknown1;
			char			name[0x80];
			unsigned long	unknown2;
			unsigned short	width;
			unsigned short	height;
			unsigned long	format;                 // 0, 1, 2, 8, 9
			unsigned long	unknown4;
			unsigned long	unknown5;
//			unsigned long	unknown6;
			unsigned long	dataOffset;
			unsigned long	unknown7;
			unsigned long	fill[7];
	} TXMP_structure;

} ;

// v FileRec.size bude celkova velkost; 
// do flaga[0] si dam velkost headeru, [1] bude offset dat v raw

// OniDatRec.flags:
// 1 - no name (name_ofs = 0)
// 2 - no data (offs & size = 0)
// 8 - ?
// 0x100 000 - ?
// 0x200 000 - ?
#endif
