#include "ma_fpk.h"

char *CMassFpk::GetIdent( void )
{
	static char Ident[] = "FPK (Civilization 4)";

	return Ident;
}

char *CMassFpk::GetExtension( void )
{
	static char Ext[] = "FPK";
	return Ext;
}

int	 CMassFpk::CheckFileType( void )
{
	if ( massf == NULL )
		return 0;

	char	buf[ 100 ];
	fseek( massf, 4, SEEK_SET );
	fread( buf, 4, 1, massf );

	if ( memcmp( buf, "FPK_", 4 ) == 0 )
		return 1;
	else
		return 0;
}

void CMassFpk::GetFileMainInfo( void )
{
	files_count = 0;

	if ( massf == NULL )
		return;

	fseek( massf, 0, SEEK_END );
	filesize = ftell( massf );

	fseek( massf, 0, SEEK_SET );
	fread( &FpkHeader, sizeof( FpkHeader ), 1, massf );
	
	files_count = FpkHeader.filecount;
	lib_start = 13;

	lib_size = 0;
	data_start = 0;
}

int	 CMassFpk::ReadRec( long num )
{
	if ( massf == NULL ) 
		return 0;
	if (( (unsigned long)num >= files_count ) && ( num != -1 ))
		return 0;
	if (( num == -1 ) && 
		(( ! next_set ) || ((unsigned long)next_rec_num >= files_count )) )
		return 0;
	
	memset( &FileRec, 0, sizeof( FileRec ) );


	if ( num == -1 ) {
		if ( fseek( massf, next_rec_pos, SEEK_SET ) != 0 )
			return 0;
		num = next_rec_num;
		pos = next_rec_num - 1;
	} else {
		if ( fseek( massf, lib_start, SEEK_SET ) != 0 )
			return 0;
		pos = 0;
	}

	//read
	do {
		if ( fread( &FpkRec.namelen, 4, 1, massf ) != 1 )
			return 0;

		if ( FpkRec.namelen > FileNameWithPathMaxLen-1 ) {
			FpkRec.namelenskip = FpkRec.namelen - (FileNameWithPathMaxLen-1);
			FpkRec.namelen = (FileNameWithPathMaxLen-1);
			fseek( massf, FpkRec.namelenskip, SEEK_CUR );
		}

		fread( FpkRec.name, FpkRec.namelen, 1, massf );
		FpkRec.name[ FpkRec.namelen ] = 0;

		for( FpkRec.namelenskip=0; FpkRec.namelenskip<FpkRec.namelen; FpkRec.namelenskip++ )
			FpkRec.name[ FpkRec.namelenskip ]--;

		if ( fread( FpkRec.u1, 2, 1, massf ) != 1 )
			return 0;

/*
//		if (( FpkRec.u1[ 1 ] == 0 ) || (( FpkRec.u1[ 0 ] & 0x07 ) == FpkRec.u1[ 0 ] )) {
		if (( FpkRec.u1[ 1 ] == 0 ) || ( FpkRec.u1[ 0 ] == 1 )) {
			FpkRec.u3_bytes = FpkRec.u1[ 0 ];

			if ( fread( FpkRec.u3, FpkRec.u3_bytes, 1, massf ) != 1 )
				return 0;
		} else
			FpkRec.u3_bytes = 0;
*/
		// I don't know how to properly read the data block after the name, it's 
		// length definition is weird, so I try to skip it.
		memset( FpkRec.u3, 0, 2 );
		while ( memcmp( FpkRec.u3, "\xC5\x01", 2 ) != 0 ) {
			
			FpkRec.u3[ 0 ] = FpkRec.u3[ 1 ];
			fread( &FpkRec.u3[ 1 ], 1, 1, massf );

		} ;

//		fread( FpkRec.u2, 6, 1, massf );

		fread( &FpkRec.fsize, 4, 1, massf );
		fread( &FpkRec.offset, 4, 1, massf );

		pos++;
	} while ( pos < num );


	//set position of the next
	next_rec_pos = ftell( massf );

	next_rec_num = num + 1;
	if ( num + 1 >= (long)files_count )
		next_set = false;
	else
		next_set = true;
	
	//get info
	FileRec.SetName( FpkRec.name );
	FileRec.offset = FpkRec.offset;
	FileRec.size = FpkRec.fsize;
	FileRec.set = 1;

/*
	char	buf[ 1024 ];
	sprintf( buf, " %02X %02X,6: %02X %02X %02X %02X %02X %02X", 
		FpkRec.u1[ 0 ], FpkRec.u1[ 1 ],
		FpkRec.u2[ 0 ], FpkRec.u2[ 1 ], FpkRec.u2[ 2 ], FpkRec.u2[ 3 ], FpkRec.u2[ 4 ], FpkRec.u2[ 5 ] );
	strcat( FileRec.name, buf );

	for( FpkRec.namelenskip=0; FpkRec.namelenskip<FpkRec.u3_bytes; FpkRec.namelenskip++ ) {
		sprintf( buf, ":%02X", FpkRec.u3[ FpkRec.namelenskip ] );
		strcat( FileRec.name, buf );
	}
*/
	return 1;
}

/*

various found data blocks following the file name:

0C DF E7 9B 4D D1 C5 01 
13 19 1A 51 4D D1 C5 01 
6D 64 25 EF 4C D1 C5 01 
D8 D5 FD 6D 4D D1 C5 01  
02 ED 88 DD 4C D1 C5 01 
66 22 E0 A1 4D D1 C5 01 
01 5F 36 D4 4C D1 C5 01 
01 C9 D3 7E 4B 4D D1 C5 01 
02 00 09 40 CF C1 4C D1 C5 01  
02 00 09 40 CF C1 4C D1 C5 01 
02 00 A0 A9 19 A7 4D D1 C5 01 
02 00 35 99 D7 CD 4C D1 C5 01 
02 00 70 5D 82 CF 4C D1 C5 01 
03 00 00 BE 60 90 E2 4C D1 C5 01 
03 00 00 79 81 C4 F9 7A D2 C5 01 
03 00 00 CA 4D D7 D8 4C D1 C5 01 

*/
