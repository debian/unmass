#ifndef _cmass_roll_h
#define _cmass_roll_h

#include "ma.h"

class CMassRoll : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next
	unsigned long Extract( unsigned long offset, unsigned long size, unsigned char *output );

	struct s_rollRec {
		unsigned long	offset;
		unsigned long	size;
		unsigned long   u1;
		unsigned long	u2;
	} rollRec;	// 16

	FILE		*ImgFile;
	char		ImgFileName[ FileNameWithPathMaxLen ];

	#define SizeOfRollRec 16
} ;

#endif
