#ifndef _cmass_moor3_h
#define _cmass_moor3_h

#include "ma.h"

class CMassMoor3 : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next
	int  Extract( unsigned char *output );

	#define moor3NameLen			48

	struct s_moor3Rec {
		char			name[ moor3NameLen ];
		unsigned long   offset;
		unsigned long	size;
		unsigned long	u[2];
	} moor3Rec; // 64

	#define SizeOfMoor3Rec 64
} ;

#endif
