#ifndef _cmass_GunMetal_h
#define _cmass_GunMetal_h

#include "ma.h"

class CMassGunMetal : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next
	unsigned long Extract( 
		unsigned long offset, unsigned long size, unsigned char *output );

	int	xored_data;

	struct s_gunmetalRec {
		unsigned long	hash;
		unsigned long	offset;
		unsigned long	size;
	} gunmetalRec;

} ;

#endif
