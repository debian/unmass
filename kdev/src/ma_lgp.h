#ifndef _cmass_lgp_h
#define _cmass_lgp_h

#include "ma.h"

class CMassLgp : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next
	int  Extract( unsigned char *output );

	#define lgpNameLen              20

	struct s_lgpRec {
		char            name[ lgpNameLen ];
		unsigned long   offset;
		char            info[3];
	} lgpRec;       //len:27

	#define SizeOfLgpRec 27
} ;

#endif
