/*
    Unmass - unpacker for game archives.
    Copyright (C) 2002-2007  Mirex

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// created by mirex (http://mirex.mypage.sk)
// used to unpack big mass files used in various games
#ifndef _cmassfiles_h_
#define _cmassfiles_h_

#include <stdio.h>
#include <memory.h>
#include <malloc.h>

#include "ma.h"

/*
	Wrapper class to ease the working with archives.

	Contains array of all the archive plugins and finds out proper one to work with.

*/

class CMassFiles {
private:
			CMassFiles( const CMassFiles& );
public:
			CMassFiles( void );
			~CMassFiles();

			// opens the file, tests its file type
			//@returns 1 when succesfully opened, 0 on no file or unknown file type
	int		Open( const char *MassFileName );
			
			// the same as CMassArchive::ReopenForWriting()
			//@returns 1 - success 0 - failed
	int		ReopenForWriting( void );

			// closes the file
	void	Close( void );

			// returns file type ident
			//@type : value from enum [massftypes]
	char*	GetTypeIdentString( int type );

			// returns file type extension
			//@type : value from enum [massftypes]
	char*	GetTypeExt( int type );

			// the same as CMassArchive::GetRec()
	int		GetRec( long num );

			// the same as CMassArchive::ReadRecords()
	int		ReadRecords( void );

			// calls ReadRecords for all files
	int		ReadAllRecords( void );

			// extracts single file
			// takes all parameters from [FileRec] local variable,
			// so you should call Extract() right after ReadRec()
			// you can change target filename by modifying [FileRec.name]
	int		Extract( void );

			// adds the file into the archive
			//@filename	: file added to archive
			//@archive_name	: new file name displayed in the archive
			//@returns : file index
	int		AddFile( char* filename, char* archive_name );

			// the same as CMassArchive::UnlinkFile()
	int		UnlinkFile( unsigned long index );

			// the same as CMassArchive::CreateNewArchive()
			//@type : value from enum [massftypes]
			//@returns 1 - success 0 - failed
	int		CreateNewArchive( char* newfilename, int type );

			// converts file type extensions for example "exe,bat" to string 
			// which can be used as windows file dialog extension filter "*.exe;*.bat"
	const char*	TypeExtToWinDlgExt( int type );

			// gets first extension from the type extensions list. if list 
			// would be "exe,bat", "exe" will be returned
	const char*	GetTypeFirstExt( unsigned long type );

			// single file information instance
	CMassArchive::s_FileRec	FileRec;

	// list of the types supported in this class. All this classes are instanciated
	// in constructor of this class, and filled into Archive array.
	enum massftypes {
		massftype_lgp,
		massftype_pak,
		massftype_wad2,
		massftype_ipwad,
		massftype_grp,
		massftype_lbx,
		massftype_vol,
		massftype_wtn,
		massftype_dune2,
		massftype_moor3,
		massftype_swine,
		massftype_roll,
		massftype_umod,
		massftype_pbo,
		massftype_crismon,
		massftype_ff8,
		massftype_bif,
		massftype_eth2,
		massftype_oni_dat,
		massftype_ecou,
		massftype_vf1bin,
		massftype_megamanL,
		massftype_mgs, 
		massftype_mea,
		massftype_gunmetal,
		massftype_fpk,
		massftypes_count,
		massftype_unknown = massftypes_count
	} ;

	CMassArchive	*Archive[ massftypes_count ];

	enum {
		FileNameWithPathMaxLen		= 256,
	} ;

	// information on the opened file
	struct {
		// directory of the file
		char			filedir[FileNameWithPathMaxLen+1];
		//mass file name (without dir&ext)
		char			filename[FileNameWithPathMaxLen+1];
		//mass file extension
		char			fileextension[20];
		//parameter given
		char			filenm[FileNameWithPathMaxLen+1];
		//files in mass file
		unsigned long	files_count;
		//filetype
		int				type;
		//valid data
		int				set;

		char			typestring[FileNameWithPathMaxLen+1];

		int				fills_type;

		unsigned long	flags;
	} MassfInfo;

private:

	// creates directories in mentioned path if they don't exist
	void MakeDir( char *path );


	enum { ErrorStrLen = 256 } ;
public:

	// sets error string
	void SetErrorStr( const char* str )
		{ strncpy( error, str, ErrorStrLen ); }

	const char* GetErrorStr( void )
		{ return error; }

private:
	char	error[ ErrorStrLen ];
	char	str[ 1024 ];

} ;


#endif
