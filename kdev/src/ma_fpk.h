#ifndef _cmass_fpk_h_
#define _cmass_fpk_h_

#include "ma.h"

class CMassFpk : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next
	int  Extract( unsigned char *output );

#pragma pack( push )
#pragma pack( 1 )

	struct s_FpkHeader {
		long	u1;
		long	ident;
		char	u2;
		long	filecount;
    } FpkHeader;  //len:13

	struct s_FpkRec {
		long	namelen;
		long	namelenskip;
		char	name[ FileNameWithPathMaxLen ];
		unsigned char	u1[ 2 ], u3[ 30 ], u2[ 6 ];
		long	u3_bytes;
		long	fsize;
		long	offset;
	} FpkRec;

#pragma pack( pop )

} ;

#endif
