#ifndef _cmass_ipwad_h
#define _cmass_ipwad_h

#include "ma.h"

class CMassIPwad : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next
	int  Extract( unsigned char *output );

	#define ipwadNameLen    		8

	struct s_ipwadRec {
		unsigned long   offset;
		unsigned long   size;
		char            name[ ipwadNameLen ];
    } ipwadRec;     //len:16

	#define SizeOfIPwadRec 16
} ;

#endif
