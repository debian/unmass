#ifndef _cmass_ecou_h
#define _cmass_ecou_h

#include "ma.h"

class CMassEcou : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next

	struct s_EcouRec {
		unsigned char	name_len;
		char			name[ FileNameWithPathMaxLen ];
		unsigned char	size_c_len;
		char			size_c[ FileNameWithPathMaxLen ];
		unsigned long	size;
	} EcouRec;

	unsigned long	next_file_offset;
	
} ;

#endif
