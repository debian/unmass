/*
    Unmass - unpacker for game archives.
    Copyright (C) 2002-2007  Mirex

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _utools_h_
#define _utools_h_

#include <vector>

#include "massfs.h"

class UnmassTools {

public:

	// prints text into console
	static void Log( int severity, const char* format, ... );

	// main routine. parses command line and processes it
	int Start( int argc, char* argv[] );

	// prints help into console
	void PrintHelp( void );

	// prints main program information
	void PrintProgramInfo( void );

	// prints list of plugin modules
	void PrintModules( void );

	int PrintMassFileList( const char* mass_filename );

	typedef std::vector< const char* >	array_of_filenames;
	// expects number_of_files * "const char* filename" in the "..."
	// filename can be either "name" or "nam*" or "*me" or "na*me" or "*"
	int ExtractMultipleFiles( const char* mass_filename, array_of_filenames names );

	// calling platform - dependant solutions of the functions
	// lowercases whole string
	static void _strlwr( char* string );

	// compares strings case insensitive
	static int _strcasecmp( const char* s1, const char* s2 );

	// compares strings case insensitive, compares only first n characters
	int _strncasecmp( const char* s1, const char* s2, int n );

	// list of possible command-line parameters
	struct s_params {
		int		ident;
		char	string[ 20 ];
		int		param_count;
	} ;

	enum e_params {
		param_help,
		param_list_of_modules,
		param_print_file_list,
		param_extract_files,

		param_count,
		param_none
	} ;

	enum {
		param_def_count = 6,
	} ;

	// data of the parameter definitions
	static s_params	param_def[ param_def_count ];
	

	// version string of the code
	static	char version_string[ 20 ];
	static	char executable_name_string[ 20 ];


	CMassFiles	massfs;

} ;


#endif
