#ifndef _cmass_grp_h
#define _cmass_grp_h

#include "ma.h"

class CMassGrp : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next
	int  Extract( unsigned char *output );

	#define grpNameLen              12

	struct s_grpRec {
			char            name[ grpNameLen ];
			unsigned long   size;
	} grpRec;       //len:16

	#define SizeOfGrpRec 16
} ;

#endif
