#ifndef _cmass_mgs_h
#define _cmass_mgs_h

#include "ma.h"

// Metal gear solid

class CMassMgs : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next

	struct s_MgsRec {
		char			name[ FileNameWithPathMaxLen ];
		unsigned long	size;
		unsigned long	offset;
	} MgsRec;

	int				i, p;
	unsigned char	uc;
} ;

#endif
