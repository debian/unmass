#ifndef _cmass_lbx_h
#define _cmass_lbx_h

#include "ma.h"

class CMassLbx : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next
	int  Extract( unsigned char *output );

	#define lbxNameLen				12

	struct s_lbxRec {
		char			name[ lbxNameLen ];
		unsigned long	offset;
		unsigned long	size;
	} lbxRec;		//len:20

	#define SizeOfLbxRec 20
} ;

#endif
