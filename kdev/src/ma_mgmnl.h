#ifndef _cmass_MgmanLegends_h
#define _cmass_MgmanLegends_h

#include "ma.h"

class CMassMegamanLegends : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next

	unsigned long	l, l2;
	char			buf[ 16 ];
} ;

#endif
