#include "ma_pak.h"

char *CMassPak::GetIdent( void )
{
	static char Ident[] = "PAK (Quake)";

	return Ident;
}

char *CMassPak::GetExtension( void )
{
	static char Ext[] = "pak";
	return Ext;
}

int	 CMassPak::CheckFileType( void )
{
	if ( massf == NULL )
		return 0;

	char	buf[ 100 ];
	fseek( massf, 0, SEEK_SET );
	fread( buf, 4, 1, massf );

	if ( memcmp( buf, "PACK", 4 ) == 0 )
		return 1;
	else
		return 0;
}

void CMassPak::GetFileMainInfo( void )
{
	files_count = 0;

	if ( massf == NULL )
		return;

	fseek( massf, 0, SEEK_END );
	filesize = ftell( massf );

	fseek( massf, 4, SEEK_SET );
	fread( &lib_start, 4, 1, massf );
	fread( &files_count, 4, 1, massf );
	files_count /= 64;
}

int	 CMassPak::ReadRec( long num )
{
	if ( massf == NULL ) 
		return 0;
	if (( (unsigned long)num >= files_count ) && ( num != -1 ))
		return 0;
	if (( num == -1 ) && 
		(( ! next_set ) || ((unsigned long)next_rec_num >= files_count )) )
		return 0;
	
	memset( &FileRec, 0, sizeof( FileRec ) );

	//seek
	if ( num == -1 ) {
		if ( fseek( massf, next_rec_pos, SEEK_SET ) != 0 )
			return 0;
	}
	else {
		pos = lib_start + SizeOfPakRec * num;
		if ( fseek( massf, pos, SEEK_SET ) != 0 )
			return 0;
	}

	//read
	if ( fread( &pakRec, SizeOfPakRec, 1, massf ) != 1 )
		return 0;

	//set position of the next
	next_rec_pos = ftell( massf );
	if ( num == -1 )
		next_rec_num ++;
	else
		next_rec_num = num + 1;
	if ( num + 1 >= (long)files_count )
		next_set = false;
	else
		next_set = true;
	
	//get info
	FileRec.SetName( pakRec.name );
	FileRec.offset = pakRec.offset;
	FileRec.size = pakRec.size;
	return 1;
}

