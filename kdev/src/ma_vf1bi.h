#ifndef _cmass_vf1bit_h
#define _cmass_vf1bit_h

#include "ma.h"

class CMassVF1bin : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next

#define BufSize	1000
	unsigned char	buffer[ BufSize ];
} ;

#endif
