#ifndef _cmass_eth2_h
#define _cmass_eth2_h

#include "ma.h"

class CMassEth2 : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next

#pragma pack( push )
#pragma pack( 1 )
	struct s_Eth2Rec {
		unsigned long	u1, size, offset, u2;
		unsigned short	namelen;
		unsigned long	nameoffset;
	} Eth2Rec;       //len:22
#pragma pack( pop )

	#define SizeOfEth2Rec 22
} ;

#endif
