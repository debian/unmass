#ifndef _cmass_ff8_h
#define _cmass_ff8_h

#include "ma.h"

class CMassFF8 : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next

	struct s_fiRec {
		unsigned long		size;
		unsigned long		offset;
		unsigned long		packed;
	} fiRec; //len:12

	#define SizeOfFiRec 12


	unsigned long	next_name_rec_pos;
	FILE			*fl;
	char			fsName[ FileNameWithPathMaxLen ];
	char			flName[ FileNameWithPathMaxLen ];

	struct {
		unsigned long   next_rec_num, next_rec_pos;
		bool			next_set;
		unsigned long	next_name_rec_pos;
	} save;


	
	CMassFF8() 
		{ massf = NULL; files_count = 0; next_set = false; FileRec.set = 0; fl = NULL; }
	~CMassFF8()
		{ if ( massf != NULL ) fclose( massf ); massf = NULL; \
			if ( fl != NULL ) fclose( fl ); fl = NULL; }
	
	unsigned long	Extract( unsigned long offset, unsigned long size, unsigned char *output );
	int				GetFlags( void );
	int				ExtractWholeFile( FILE *fout );
	
//	void SaveNextPos( void );
//	void LoadNextPos( void );

	unsigned long ex_last_ofs, ex_last_pos;

} ;

#endif
