#ifndef _cmass_pak_h
#define _cmass_pak_h

#include "ma.h"

class CMassPak : public CMassArchive {
public:

	char *GetIdent( void );
	char *GetExtension( void );
	int	 CheckFileType( void );
	void GetFileMainInfo( void );
	int	 ReadRec( long num );	//num = -1 => next
	int  Extract( unsigned char *output );

	#define pakNameLen              56

	struct s_pakRec{
            char            name[ pakNameLen ];
			unsigned long   offset;
            unsigned long   size;
    } pakRec;       //len:64

	#define SizeOfPakRec 64
} ;

#endif
