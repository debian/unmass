// ChArchType.cpp : implementation file
//

#include "stdafx.h"
#include "Unmassw.h"
#include "ChArchType.h"

#include <assert.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChArchType dialog


CChArchType::CChArchType(CWnd* pParent /*=NULL*/)
	: CDialog(CChArchType::IDD, pParent)
{
	//{{AFX_DATA_INIT(CChArchType)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CChArchType::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChArchType)
	DDX_Control(pDX, IDC_COMBOtype, m_ArchiveType);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CChArchType, CDialog)
	//{{AFX_MSG_MAP(CChArchType)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChArchType message handlers

BOOL CChArchType::OnInitDialog() 
{
	CDialog::OnInitDialog();

	assert( strings.GetSize() == data.GetSize() );

	for( int i=0; i<strings.GetSize(); i++ )
		m_ArchiveType.SetItemData(
			m_ArchiveType.AddString( strings.GetAt( i ) ),
			data.GetAt( i ) );
	
	m_ArchiveType.SetCurSel( 0 );
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CChArchType::OnDestroy() 
{
	choice = m_ArchiveType.GetCurSel();
	if ( choice != CB_ERR )
		choice = m_ArchiveType.GetItemData( choice );
	
	CDialog::OnDestroy();
		
}
